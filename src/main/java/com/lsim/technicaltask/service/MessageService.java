package com.lsim.technicaltask.service;

import com.lsim.technicaltask.model.MessageRequest;
import com.lsim.technicaltask.model.entity.Message;

import java.util.List;

public interface MessageService {
    void createMessage(MessageRequest messageRequest);
    List<Message> getAllAvailableMessages();
    void updateMessage(Message message);
}
