package com.lsim.technicaltask.service.impl;

import com.lsim.technicaltask.model.MessageRequest;
import com.lsim.technicaltask.model.dto.ResponseDTO;
import com.lsim.technicaltask.model.entity.Message;
import com.lsim.technicaltask.repository.MessageRepository;
import com.lsim.technicaltask.service.ClientService;
import com.lsim.technicaltask.service.MessageService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;
    private final ClientService clientService;

    public MessageServiceImpl(MessageRepository messageRepository, ClientService clientService) {
        this.messageRepository = messageRepository;
        this.clientService = clientService;
    }

    @Override
    public void createMessage(MessageRequest messageRequest) {
        Message message = new Message();

        message.setMsisdn(messageRequest.getMsisdn());
        message.setSenderName(messageRequest.getSenderName());
        message.setMessageBody(messageRequest.getMessageBody());

        messageRepository.save(message);
    }

    @Override
    public List<Message> getAllAvailableMessages(){
        return messageRepository.findAllAvailableMessages();
    }

    @Override
    public void updateMessage(Message message) {
        ResponseDTO responseDTO = clientService.receive(message.getMsisdn(),
                                         message.getSenderName(), message.getMessageBody());
        if (responseDTO.getCode() == 200) {
            message.setSent(true);
            message.setDoneDate(LocalDateTime.now());
            messageRepository.save(message);
        }
    }
}
