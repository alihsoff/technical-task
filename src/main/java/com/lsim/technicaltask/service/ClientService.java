package com.lsim.technicaltask.service;

import com.lsim.technicaltask.model.dto.ResponseDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "ms-client", url = "localhost:8080")
public interface ClientService {
    @GetMapping("/sms/send")
    ResponseDTO receive(@RequestParam("msisdn") String msisdn,
                               @RequestParam("sender") String senderName,
                               @RequestParam("text") String messageBody);
}
