package com.lsim.technicaltask.listener;

import com.lsim.technicaltask.model.entity.Message;
import com.lsim.technicaltask.service.MessageService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class Listener {
    private static final ExecutorService es = Executors.newCachedThreadPool();
    private final MessageService messageService;

    public Listener(MessageService messageService) {
        this.messageService = messageService;
    }

    @Scheduled(fixedRate = 1000)
    public void checkDatabase(){
    List<Message> messageList = messageService.getAllAvailableMessages();
        for (Message message : messageList)
            es.submit(new Runnable() {
                @Override
                public void run() {
                    messageService.updateMessage(message);
                }
            });
    }
}
