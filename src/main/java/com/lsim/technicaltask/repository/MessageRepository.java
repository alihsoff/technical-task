package com.lsim.technicaltask.repository;

import com.lsim.technicaltask.model.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, Long> {

    @Query("SELECT m FROM Message m WHERE m.isSent = false and datediff(curdate(), m.insertDate)<=5")
    List<Message> findAllAvailableMessages();
}
