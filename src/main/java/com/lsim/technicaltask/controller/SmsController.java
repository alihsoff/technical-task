package com.lsim.technicaltask.controller;

import com.lsim.technicaltask.model.MessageRequest;
import com.lsim.technicaltask.service.MessageService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class SmsController {

    private final MessageService messageService;

    public SmsController(MessageService messageService) {
        this.messageService = messageService;
    }

    @PostMapping("/send")
    public ResponseEntity<String> send(@RequestBody @Valid MessageRequest messageRequest){
        messageService.createMessage(messageRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
