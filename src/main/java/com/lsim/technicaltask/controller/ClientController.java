package com.lsim.technicaltask.controller;

import com.lsim.technicaltask.model.dto.ResponseDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sms")
public class ClientController {
    @GetMapping("/send")
    public ResponseDTO receive(@RequestParam("msisdn") String msisdn,
                       @RequestParam("sender") String senderName,
                       @RequestParam("text") String messageBody){
    return ResponseDTO.builder()
            .code(200)
            .message("SUCCESS")
            .build();
    }
}
