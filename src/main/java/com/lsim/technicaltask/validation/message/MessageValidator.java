package com.lsim.technicaltask.validation.message;

import com.lsim.technicaltask.model.MessageRequest;
import com.lsim.technicaltask.util.CheckViolationHelper;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class MessageValidator implements
        ConstraintValidator<MessageConstraint, MessageRequest> {

    private final CheckViolationHelper checkViolationHelper;

    public MessageValidator(CheckViolationHelper checkViolationHelper) {
        this.checkViolationHelper = checkViolationHelper;
    }

    @Override
    public boolean isValid(MessageRequest messageRequest, ConstraintValidatorContext context) {
        return messageRequest != null &&
                isMsisdnValid(messageRequest.getMsisdn(), context);
    }

    private boolean isMsisdnValid(String msisdn, ConstraintValidatorContext context) {
        if (msisdn == null || msisdn.isEmpty()) {
            checkViolationHelper.addViolation(context, "msisdn", "Phone number cannot be empty");
            return false;
        }
        if(msisdn.length() != 12){
            checkViolationHelper.addViolation(context, "msisdn", "Number length is not correct");
            return false;
        }
        String areaCode = msisdn.substring(0, 3);
        String oprCode = msisdn.substring(3, 5);
        if(!areaCode.equals("994") || (!oprCode.equals("10") && !oprCode.equals("50") && !oprCode.equals("51")
                && !oprCode.equals("55") && !oprCode.equals("60") && !oprCode.equals("70") && !oprCode.equals("77")
                && !oprCode.equals("99"))){
            checkViolationHelper.addViolation(context, "msisdn", "Format of phone number is wrong");
            return false;
        }
        return true;
    }
}