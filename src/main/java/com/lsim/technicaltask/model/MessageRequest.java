package com.lsim.technicaltask.model;

import com.lsim.technicaltask.validation.message.MessageConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@MessageConstraint
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageRequest {
    private String msisdn;
    private String senderName;
    private String messageBody;
}
