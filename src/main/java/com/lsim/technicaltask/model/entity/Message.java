package com.lsim.technicaltask.model.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String msisdn;

    private String senderName;

    private String messageBody;

    @CreationTimestamp
    private LocalDateTime insertDate;

    private LocalDateTime doneDate;

    @Column(columnDefinition="boolean default false")
    private boolean isSent;

    @Column(columnDefinition="int default 1")
    private byte status;
}
